function myFunction() {
    var x = document.getElementById("phoneMenu");
    if (x.style.display === "block") {
      x.style.display = "none";
      document.getElementById('crossImage').src = 'images/icon-hamburger.svg'
      document.getElementById('crossImage').style.transition = '1s'
    } else {
      x.style.display = "block";
       document.getElementById('crossImage').src = 'images/icon-close.svg'
       document.getElementById('crossImage').style.transition = '1s'
    }
  }